<header id="header" class="container">
	<nav class="navbar navbar-expand-lg navbar-light">
		<a class="navbar-brand" href="#">
			<img src="/storage/logo/h1.jpg" width="30" height="30" class="d-inline-block align-top" alt="Jorgedipra">
			Jorgedipra <i class="fas fa-cogs"></i>
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<span id="space"></span>
			<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
				<!-- <li class="nav-item active">
					<a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
				</li> -->

				<li class="nav-item">
					<a class="nav-link " href="/Portafolio"><i class="fab fas fa-tv fab-2x"></i>Portafolio</a>
				</li>
				<li class="nav-item">
					<a class="nav-link " href="/Savelink"><i class="fab fas fa-link fab-2x"></i>Savelink</a>
				</li>
				<li class="nav-item">
					<a class="nav-link " href="/Contacto"><i class="fab fas fa-envelope fab-2x"></i>Contactame</a>
				</li>
			</ul>
			<button class="btn btn-secondary my-2 my-sm-0" type="submit"><i class="fab fas fa-user-circle fab-2x"></i>Login</button>
		</div>
	</nav>
</header>