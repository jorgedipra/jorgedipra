    <footer>
      <div class="footer-top"> 
       <div class="container">
        <ul class="menu"> 
         <li>
          <a href="https://www.facebook.com/jorgedipra" target="bank"><i class="fab fa-facebook fab-2x"></i>Facebook</a>
        </li> 
        <li>  
         <a href="https://twitter.com/jorgedipra" target="bank"><i class="fab fa-twitter fab-2x"></i>Twitter</a>
       </li>                                   
       <li>
        <a href="https://plus.google.com/u/0/+jorgeedwindiazprado" target="bank"><i class="fab fa-google-plus fab-2x"></i>Google</a>
      </li>                                   
      <li>
        <a href="http://steamcommunity.com/id/jorgedipra" target="bank"><i class="fab fa-steam fab-2x"></i>Steam</a>
      </li> 
       <li>
        <a href="https://www.youtube.com/user/jorgedipra" target="bank"><i class="fab fa-youtube fab-2x"></i>Youtube</a>
      </li>                                   
      <li>
        <a href="https://github.com/jorgedipra" target="bank"><i class="fab fa-github-alt fab-2x"></i>Github</a>
      </li>
      <li>
        <a href="https://bitbucket.org/jorgedipra" target="bank"><i class="fab fa-bitbucket fab-2x"></i>Bitbucket</a>
      </li>
    </ul>
  </div> 
</div>  
<div class="container">
  <div class="row text-center">   
    <div class="text-center menu_footer">
      <ul> 
       <li>
        <a href="/">Home</a>
      </li>
      <li>
       <a href="about">About</a>
     </li>
     <li>
      <a href="Blog">Blog</a>
    </li>
    <li>
     <a href="Galeria">Galeria</a>
   </li>
   <li>
    <a href="Estadisticas">Estadisticas</a>
  </li>
</ul>
</div>
</div>
</div>
</footer>    
<div class="copyright">
 <div class="container">
   <div class="row text-center">
    <p>Copyright © 2017 @Jorgedipra @MjplayV | All rights reserved</p>
  </div>
</div>
</div>