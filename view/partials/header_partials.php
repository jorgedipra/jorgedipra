<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta property="og:title" content="Jorgedipra" />
	<meta property="og:description" content="Web personal, donde pueden tener de primera mano la información sobre lo que hago, a que me dedicó y que he hecho" />
	<meta property="og:url" content="http://jorgedipra.hol.es" />
	<meta property="og:image" content="/storage/logo/h1.jpg" />
	<meta name="twitter:creator" content="@jorgedipra">
	<meta name="twitter:image" content="/storage/logo/h1.jpg">
	<meta name="copyright" content="© 2018">
	<meta name="author" content="Jorgedipra">
	<meta name="description" content="Web personal, donde pueden tener de primera mano la información sobre lo que hago, a que me dedicó y que he hecho">
	<meta name="keywords" content="Web,personal">
	<meta http-equiv="Content-Language" content="es"/>
	<meta name="Distribution" content="global"/>
	<meta name="Robots" content="all"/>
	<title><?=Titulo?></title>
	<link rel="shortcut icon" href="/storage/logo/h1.ico" />	
	<link rel="stylesheet" href="/public/files/bootstrap-4.0.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="/public/css/font/awesome5/css/fontawesome-all.min.css">
	<link rel="stylesheet" href="/public/css/font/roboto/roboto.css">
	<link rel="stylesheet" href="/public/css/style.css">
	<link rel="stylesheet" href="/public/css/<?=$match['name']?>.css">
</head>
<body>



